// SECTION I. Dependencies
const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;

// SECTION II. User Order Checkout
router.post('/checkOut',verify,orderControllers.checkOut);

// SECTION III. View Authenticated User Order
router.get('/viewLoginUserOrder',verify,orderControllers.viewLoginUserOrder);

// SECTION IV. View All Orders
router.get('/viewAllOrders',verify,verifyAdmin,orderControllers.viewAllOrders)

// SECTION V. View A Specific User Order/s
router.get('/viewUserOrder/:userId',verify,verifyAdmin,orderControllers.viewUserOrder)

// SECTION VI. View A Specific Order
router.get('/viewOrder/:orderId',verify,verifyAdmin,orderControllers.viewOrder)

// SECTION VII. View Products Per Order
router.get('/viewProductsPerOrder/:orderId',verify,orderControllers.viewProductsPerOrder)

// SECTION ?. Router Export
module.exports = router;