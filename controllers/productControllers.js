// SECTION I. Dependencies
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// SECTION II. Add Product
module.exports.addProduct = (req,res)=>{

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,

	})

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// SECTION III. Retrieve All Active Product
module.exports.viewAllActive = (req,res)=>{

	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// SECTION IV. Retrieve One Product
module.exports.viewOne = (req,res) => {

	Product.findOne({_id:req.params.productId})
	.then(result => {
		if(result.isActive){
			res.send(result)
		} else {
			res.send({message: "Not an Active Product."})
		}
	})
	.catch(error => res.send(error))

}

// SECTION V. Update Product Information
module.exports.updateProduct = (req,res) => {

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// SECTION VI. Archive A Product
module.exports.archiveProduct = (req,res) => {

	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// SECTION VII. Activate A Product
module.exports.activateProduct = (req,res) => {

	let update = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}